 <fieldset>
 <legend>ฟิลด์ในรูปแบบรหัส</legend>
 <p>
 <label>ผู้อ่าน</label>
 <select name="limit" class="subtype">
 <option value="" selected="selected" >ใดๆ</option>
 <option value="aud:a">เยาวชน, ทั่วไป</option>
 <option value="aud:b">ปฐมวัย (อายุ 0-5 ปี)</option>
 <option value="aud:c">ประถม (5-8)</option>
 <option value="aud:d">เด็ก (อายุ 9-14 ปี)</option>
 <option value="aud:e">วัยหนุ่มสาว</option>
 <option value="aud:k">ผู้ใหญ่, จริงจัง</option>
 <option value="aud:m">ผู้ใหญ่, ทั่วไป</option>
 <option value="aud:u">ไม่รู้จัก</option>
 </select>
 </p>
 <p>
 <label>พิมพ์</label>
 <select name="limit" class="subtype">
 <option value="" selected="selected" >ใดๆ</option>
 <option value="Material-type:r">งานพิมพ์ขนาดปกติ</option>
 <option value="Material-type:d">งานพิมพ์ขนาดใหญ่</option>
 <option value="Material-type:e">รูปแบบหนังสือพิมพ์</option>
 <option value="Material-type:f">อักษรเบรลล์หรือมูนสคริปต์</option>
 <option value="Material-type:g">งานพิมพ์ขนาดจิ๋ว</option>
 <option value="Material-type:h">เอกสารที่เขียนด้วยมือ</option>
 <option value="Material-type:i">สื่อผสม</option>
 <option value="Material-type:j">งานพิมพ์ขนาดเล็ก</option>
 <option value="Material-type:s">ทรัพยากรอิเล็กทรอนิกส์</option>
 <option value="Material-type:t">วัสดุย่อส่วน</option>
 <option value="Material-type:z">วัสดุที่สื่อข้อความในรูปแบบอื่น</option>
 </select>
 </p>
 <p>
 <label>ประเภทบทประพันธ์</label>
 <select name="limit" class="subtype">
 <option value="" selected="selected" >ใดๆ</option>
 <option value="Literature-Code:a">นิยาย</option>
 <option value="Literature-Code:b">บทละคร</option>
 <option value="Literature-Code:c">เรียงความ</option>
 <option value="Literature-Code:d">ตลกขบขัน, เสียดสี</option>
 <option value="Literature-Code:e">จดหมาย</option>
 <option value="Literature-Code:f">เรื่องสั้น</option>
 <option value="Literature-Code:g">บทกวี</option>
 <option value="Literature-Code:h">คำปราศรัย, สุนทรพจน์</option>
 <option value="Literature-Code:i">บทร้อง</option>
 <option value="Literature-Code:y">ไม่ใช่บทประพันธ์</option>
 <option value="Literature-Code:z">วรรณกรรมหลายรูปแบบ/รูปแบบอื่นๆ</option>
 </select>
 </p>
 <p>
 <label>ชีวประวัติ</label>
 <select name="limit" class="subtype" size="1">
 <option value="">ใดๆ</option>
 <option value="Biography-code:y">ไม่ใช่ชีวประวัติ</option>
 <option value="Biography-code:a">อัตชีวประวัติ</option>
 <option value="Biography-code:b">ชีวประวัติบุคคลคนเดียว</option>
 <option value="Biography-code:c">ชีวประวัติรวมบุคคล</option>
 <option value="Biography-code:d">มีข้อมูลชีวประวัติ</option>
 </select>
 </p>
 <p>
 <label>ภาพประกอบ</label>
 <select name="limit" class="subtype" size="1">
 <option value="">ใดๆ</option>
 <option value="Illustration-Code:a">ภาพวาด</option>
 <option value="Illustration-Code:b">แผนที่</option>
 <option value="Illustration-Code:c">ภาพคน</option>
 <option value="Illustration-Code:d">แผนภูมิ</option>
 <option value="Illustration-Code:e">แผนงาน</option>
 <option value="Illustration-Code:f">โล่หรือแผ่นพิมพ์</option>
 <option value="Illustration-Code:g">ดนตรี</option>
 <option value="Illustration-Code:h">โทรสาร</option>
 <option value="Illustration-Code:i">ตราประจำตระกูล</option>
 <option value="Illustration-Code:j">ตารางลำดับวงศ์</option>
 <option value="Illustration-Code:k">แบบฟอร์ม</option>
 <option value="Illustration-Code:l">ตัวอย่าง</option>
 <option value="Illustration-Code:m">เครื่องบันทึุกเสียง</option>
 <option value="Illustration-Code:n">แผ่นใส</option>
 <option value="Illustration-Code:o">ภาพระบายสี</option>
 <option value="Illustration-Code:y">ไม่มีภาพประกอบ</option>
 </select>
 </p>
 <p>
 <label>เนื้อหา</label>
 <select name="limit" class="subtype">
 <option value="" >ใดๆ</option>
 <option value="ctype:a" >บรรณานุกรม</option>
 <option value="ctype:b" >บัญชีรายชื่อ</option>
 <option value="ctype:c" >ดัชนี</option>
 <option value="ctype:d" >สาระสังเขป</option>
 <option value="ctype:e" >พจนานุกรม</option>
 <option value="ctype:f" >สารานุกรม</option>
 <option value="ctype:g" >สมุดรายนาม</option>
 <option value="ctype:h" >รายละเีีอียดโครงการ</option>
 <option value="ctype:i" >สถิติ</option>
 <option value="ctype:j" >แบบเรียนสำเร็จรูป</option>
 <option value="ctype:k" >สิทธิบัตร</option>
 <option value="ctype:l" >มาตรฐาน</option>
 <option value="ctype:m" >วิทยานิพนธ์ระดับปริญญาโทหรือเอก</option>
 <option value="ctype:n" >กฎหมายและระเบียบข้อบังคับ</option>
 <option value="ctype:o" >ตารางตัวเลข</option>
 <option value="ctype:p" >รายงานทางเทคนิค</option>
 <option value="ctype:q" >ข้อสอบ</option>
 <option value="ctype:r" >การสำรวจ/ทบทวนวรรณกรรม</option>
 <option value="ctype:s" >สนธิสัญญา</option>
 <option value="ctype:t" >การ์ตูนหรือการ์ตูนสั้นเป็นตอนๆ</option>
 <option value="ctype:v" >ปริญญานิพนธ์หรือวิทยานิพนธ์ (ปรับปรุง)</option>
 <option value="ctype:w" >คัมภีร์</option>
 <option value="ctype:z" >วัสดุอื่นๆ </option>
 </select>
 </p>
 <p>
 <label>ประเภทวิดีโอ</label>
 <select name="limit" class="subtype">
 <option value="">ใดๆ</option>
 <option value="Video-mt:a">ภาพยนตร์</option>
 <option value="Video-mt:b">การฉายภาพเสมือนจริง (Visual Projection)</option>
 <option value="Video-mt:c">เครื่องบันทึกภาพ</option>
 </select>
 </p>
 </fieldset>
 <fieldset>
 <legend>สิ่งพิมพ์ต่อเนื่อง</legend><p>
 <p>
 <label>ชนิดของสิ่งพิมพ์ต่อเนื่อง</label>
 <select name="limit" class="subtype">
 <option value="">ชนิดใดๆ</option>
 <option value="Type-Of-Serial:a">สิ่งพิมพ์ต่อเนื่อง</option>
 <option value="Type-Of-Serial:b">หนังสือชุด</option>
 <option value="Type-Of-Serial:c">หนังสือพิมพ์</option>
 <option value="Type-Of-Serial:e">กำลังปรับปรุงแผ่นปลิว</option>
 <option value="Type-Of-Serial:f">ฐานข้อมูล</option>
 <option value="Type-Of-Serial:g">กำลังปรับปรุงเว็บไซต์</option>
 <option value="Type-Of-Serial:z">อื่นๆ</option>
 </select>
 </p>
 <p>
 <label>ความสม่ำเสมอ</label>
 <select name="limit" class="subtype">
 <option value="">ใดๆ</option>
 <option value="Frequency-code:a">ทุกวัน</option>
 <option value="Frequency-code:b">อาทิตย์ละ 2 ครั้ง</option>
 <option value="Frequency-code:c">ทุกสัปดาห์</option>
 <option value="Frequency-code:d">รายปักษ์</option>
 <option value="Frequency-code:e">เดือนละ 2 ครั้ง (รายปักษ์)</option>
 <option value="Frequency-code:f">รายเดือน</option>
 <option value="Frequency-code:g">ทุกสองสัปดาห์</option>
 <option value="Frequency-code:h">ทุก 3 เดือน</option>
 <option value="Frequency-code:i">สามครั้งต่อปี</option>
 <option value="Frequency-code:j">ทุก 6 เดือน</option>
 <option value="Frequency-code:k">ประจำปี</option>
 <option value="Frequency-code:l">ทุกสองปี</option>
 <option value="Frequency-code:m">ทุกสามปี</option>
 <option value="Frequency-code:n">สามครั้งต่อสัปดาห์</option>
 <option value="Frequency-code:o">สามครั้งต่อเดือน</option>
 <option value="Frequency-code:y">ไม่สม่ำเสมอ</option>
 <option value="Frequency-code:u">ไม่รู้จัก</option>
 <option value="Frequency-code:z">อื่นๆ</option>
 </select>
 </p>
 <p>
 <label>ความถี่ในการจัดพิมพ์</label>
 <select name="limit" class="subtype">
 <option value="">ระยะใดๆ</option>
 <option value="Regularity-code:a">ปกติ</option>
 <option value="Regularity-code:b">ไม่สม่ำเสมอเป็นปกติ</option>
 <option value="Regularity-code:y">ทรัำพยากรอื่นที่อยู่นอกเกณฑ์</option>
 <option value="Regularity-code:u">ไม่รู้จัก</option>
 </select>
 </p>

 </fieldset>
 <fieldset>
 <legend>รูปภาพ</legend><p>
 <select name="limit" class="subtype">
 <option value="">ใดๆ</option>
 <option value="Graphics-type:a">ภาพปะต่อ</option>
 <option value="Graphics-type:b">ภาพวาด</option>
 <option value="Graphics-type:c">ภาพเขียน</option>
 <option value="Graphics-type:d">การผลิตซ้ำโดยใช้กลไกการสร้างภาพ (Photomechanical Reproduction)</option>
 <option value="Graphics-type:e">Photo Negative</option>
 <option value="Graphics-type:f">ภาพถ่ายที่พิมพ์ออกจากเครื่องพิมพ์ (Photo Print)</option>
 <option value="Graphics-type:h">รูปภาพ</option>
 <option value="Graphics-type:i">งานพิมพ์</option>
 <option value="Graphics-type:k">งานเขียนแบบเทคนิค</option>
 <option value="Graphics-type:z">ภาพที่ไม่ต้องฉายชนิดอื่น</option>
 </select>
 <select name="limit" class="subtype">
 <option value="">ใดๆ</option>
 <option value="Graphics-support:a">ผ้าใบ</option>
 <option value="Graphics-support:b">กระดาษบริสตอล</option>
 <option value="Graphics-support:c">กระดาษแข็ง/กระดาษเขียนภาพ</option>
 <option value="Graphics-support:d">แก้ว</option>
 <option value="Graphics-support:j">ปูนปลาสเตอร์</option>
 <option value="Graphics-support:k">ไม้อัด</option>
 <option value="Graphics-support:l">เครื่องเคลือบ</option>
 <option value="Graphics-support:m">ศิลาจารึก/แท่นหิน</option>
 <option value="Graphics-support:n">ไม้</option>
 <option value="Graphics-support:v">คอลเลกชันผสม</option>
 <option value="Graphics-support:e">วัสดุสังเคราะห์</option>
 <option value="Graphics-support:f">Skin</option>
 <option value="Graphics-support:g">สิ่งทอ</option>
 <option value="Graphics-support:h">โลหะ</option>
 <option value="Graphics-support:i">กระดาษ</option>
 <option value="Graphics-support:z">อื่นๆ</option>
 <option value="Graphics-support:u">ไม่รู้จัก</option>
 </select>
 </p>
