<option value="relevance">ความสอดคล้องกับเนื้อหา</option>
<optgroup label="ความนิยม">
[% IF ( sort_by == "popularity_dsc" ) %] <option value="popularity_dsc" selected="selected">ความนิยม (มากไปน้อย)</option>[% ELSE %] <option value="popularity_dsc">ความนิยม (มากไปน้อย)</option>
[% END %] [% IF ( sort_by == "popularity_asc" ) %] <option value="popularity_asc" selected="selected">ความนิยม (น้อยไปมาก)</option>[% ELSE %] <option value="popularity_asc">ความนิยม (น้อยไปมาก)</option>
[% END %] </optgroup>
<optgroup label="ชื่อผู้แต่ง">
[% IF ( sort_by == "author_az" || sort_by == "author_asc" ) %] <option value="author_az" selected="selected">ชื่อผู้แต่ง (A-Z, ก-ฮ)</option>
[% ELSE %] <option value="author_az">ชื่อผู้แต่ง (A-Z, ก-ฮ)</option>
[% END %] [% IF ( sort_by == "author_za" || sort_by == "author_dsc" ) %] <option value="author_za" selected="selected">ชื่อผู้แต่ง (Z-A, ฮ-ก)</option>
[% ELSE %] <option value="author_za">ชื่อผู้แต่ง (Z-A, ฮ-ก)</option>
[% END %] </optgroup>
<optgroup label="เลขเรียกหนังสือ">
[% IF ( sort_by == "call_number_asc" ) %] <option value="call_number_asc" selected="selected">เลขเรียกหนังสือ (0-9 ถึง A-Z, ก-ฮ)</option>
[% ELSE %] <option value="call_number_asc">เลขเรียกหนังสือ (0-9 ถึง A-Z, ก-ฮ)</option>
[% END %] [% IF ( sort_by == "call_number_dsc" ) %] <option value="call_number_dsc" selected="selected">เลขเรียกหนังสือ (Z-A, ก-ฮ ถึง 9-0)</option>
[% ELSE %] <option value="call_number_dsc">เลขเรียกหนังสือ (Z-A, ก-ฮ ถึง 9-0)</option>
[% END %] </optgroup>
<optgroup label="วันที่">
[% IF ( sort_by == "pubdate_dsc" ) %] <option value="pubdate_dsc" selected="selected">วันที่พิมพ์/จดลิขสิทธิ์: รายการใหม่สุดไปเก่าสุด</option>
[% ELSE %] <option value="pubdate_dsc">วันที่พิมพ์/จดลิขสิทธิ์: รายการใหม่สุดไปเก่าสุด</option>
[% END %] [% IF ( sort_by == "pubdate_asc" ) %] <option value="pubdate_asc" selected="selected">วันที่พิมพ์/จดลิขสิทธิ์: รายการเก่าสุดไปใหม่สุด</option>
[% ELSE %] <option value="pubdate_asc">วันที่พิมพ์/จดลิขสิทธิ์: รายการเก่าสุดไปใหม่สุด</option>
[% END %] [% IF ( sort_by == "acqdate_dsc" ) %] <option value="acqdate_dsc" selected="selected">วันที่จัดหา: รายการใหม่สุดไปเก่าสุด</option>
[% ELSE %] <option value="acqdate_dsc">วันที่จัดหา: รายการใหม่สุดไปเก่าสุด</option>
[% END %] [% IF ( sort_by == "acqdate_asc" ) %] <option value="acqdate_asc" selected="selected">วันที่จัดหา: รายการเก่าสุดไปใหม่สุด</option>
[% ELSE %] <option value="acqdate_asc">วันที่จัดหา: รายการเก่าสุดไปใหม่สุด</option>
[% END %] </optgroup>
<optgroup label="ชื่อเรื่อง">
[% IF ( sort_by == "title_az" || sort_by == "title_asc" ) %] <option value="title_az" selected="selected">ชื่อเรื่อง (A-Z, ก-ฮ)</option>
[% ELSE %] <option value="title_az">ชื่อเรื่อง (A-Z, ก-ฮ)</option>
[% END %] [% IF ( sort_by == "title_za" || sort_by == "title_dsc" ) %] <option value="title_za" selected="selected">ชื่อเรื่อง (Z-A, ฮ-ก)</option>
[% ELSE %] <option value="title_za">ชื่อเรื่อง (Z-A, ฮ-ก)</option>
[% END %] </optgroup>

