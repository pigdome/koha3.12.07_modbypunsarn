[% IF ( opacfacets ) %] [% IF ( facets_loop ) %] <div id="search-facets">
<h4>ปรับปรุงการค้นหาของคุณ</h4>
<ul>
 <li id="availability_facet">รายการที่ห้องสมุดมี<ul><li>[% IF ( available ) %]แสดงเฉพาะ<strong>ที่สามารถยืมได้</strong> รายการ<a href="/cgi-bin/koha/opac-search.pl?[% query_cgi |html %][% limit_cgi_not_availablity %][% IF ( sort_by ) %]&amp;sort_by=[% sort_by |html%][% END %]">แสดงรายการทั้งหมด</a>[% ELSE %]จำกัดเฉพาะ<a href="/cgi-bin/koha/opac-search.pl?[% query_cgi |html %][% limit_cgi |html %][% IF ( sort_by ) %]&amp;sort_by=[% sort_by |html%][% END %]&amp;limit=available">มีรายการอยู่ขณะนี้</a>[% END %]</li></ul>
 [% IF ( related ) %] <li>(การค้นหาที่เกี่ยวข้อง: [% FOREACH relate IN related %][% relate.related_search %][% END %])</li>[% END %] </li>
 
[% FOREACH facets_loo IN facets_loop %] [% IF facets_loo.facets.size > 0 %] <li id="[% facets_loo.type_id %]">
[% IF ( facets_loo.type_label_Authors ) %]ผู้แต่ง[% END %] [% IF ( facets_loo.type_label_Titles ) %]ชื่อเรื่อง[% END %] [% IF ( facets_loo.type_label_Topics ) %]หัวข้อ[% END %] [% IF ( facets_loo.type_label_Places ) %]ตำแหน่ง[% END %] [% IF ( facets_loo.type_label_Series ) %]ชุด[% END %] [% IF ( facets_loo.type_label_ItemTypes ) %]ชนิดรายการ[% END %] [% UNLESS ( singleBranchMode ) %] [% IF ( facets_loo.type_label_Libraries ) %]ห้องสมุด[% END %] [% END %] [% IF ( facets_loo.type_label_Location ) %]สถานที่[% END %]<ul>
 [% FOREACH facet IN facets_loo.facets %]<li><a href="/cgi-bin/koha/opac-search.pl?[% query_cgi |html %][% limit_cgi |html %][% IF ( sort_by ) %]&amp;sort_by=[% sort_by |html %][% END %]&amp;limit=[% facet.type_link_value %]:[% facet.facet_link_value %]" title="[% facet.facet_title_value |html %]">[% facet.facet_label_value %]</a> [% IF ( displayFacetCount ) %]([% facet.facet_count %])[% END %]</li>[% END %][% IF ( facets_loo.expandable ) %] <li class="showmore"><a href="/cgi-bin/koha/opac-search.pl?[% query_cgi |html %][% limit_cgi |html %][% IF ( sort_by ) %]&amp;sort_by=[% sort_by |html %][% END %][% IF ( offset ) %]&amp;offset=[% offset %][% END %]&amp;expand=[% facets_loo.expand %]#[% facets_loo.type_id %]">แสดงเพิ่มเติม</a></li>
[% END %] </ul></li>
[% END %] [% END %] </ul>
</div>
[% IF ( OPACResultsSidebar ) %] <div id="opacresultssidebar">
[% OPACResultsSidebar %] </div>
[% END %] [% END %] [% END %] 