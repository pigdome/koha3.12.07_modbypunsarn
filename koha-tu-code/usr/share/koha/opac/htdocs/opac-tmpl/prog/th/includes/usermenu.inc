[% IF ( opacuserlogin ) %][% IF ( loggedinusername ) %] <div id="menu">
<ul>
 [% IF ( userview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-user.pl">ภาพรวม</a></li>
 [% IF ( OPACFinesTab ) %] [% IF ( accountview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-account.pl">ค่าปรับ</a></li>
 [% END %] [% IF ( userupdateview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-memberentry.pl">ข้อมูลส่วนตัว</a></li>
 [% IF ( TagsEnabled ) %] [% IF ( tagsview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-tags.pl?mine=1">รายการแท็ก</a></li>
 [% END %] [% IF ( OpacPasswordChange ) %] [% IF ( passwdview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-passwd.pl">เปลี่ยนรหัสผ่านของฉัน</a></li>
 [% END %] [% IF ( ShowOpacRecentSearchLink ) %] [% IF ( searchhistoryview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-search-history.pl">ประวัติการค้นหา</a></li>
 [% END %] [% IF ( opacreadinghistory ) %] [% IF ( readingrecview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-readingrecord.pl">ประวัติการอ่าน</a></li>
 [% IF ( OPACPrivacy ) %] [% IF ( privacyview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-privacy.pl">ควาามเป็นส่วนตัวของฉัน</a></li>
 [% END %] [% END %] [% IF ( suggestion ) %] [% UNLESS ( AnonSuggestions ) %] [% IF ( suggestionsview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-suggestions.pl">ข้อเสนอแนะการสั่งซื้อ</a></li>
 [% END %] [% END %] [% IF ( EnhancedMessagingPreferences ) %] [% IF ( messagingview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-messaging.pl">การรับ-ส่งข้อความ</a></li>
 [% END %] [% IF ( virtualshelves ) %] [% IF ( listsview ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/opac-shelves.pl?display=privateshelves">รายการของฉัน</a></li>
 [% END %] </ul>
</div>
[% END %][% ELSE %][% END %] 