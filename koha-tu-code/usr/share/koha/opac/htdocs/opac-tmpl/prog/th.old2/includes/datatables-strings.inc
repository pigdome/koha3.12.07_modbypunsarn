<script type="text/javascript">
//<![CDATA[
    var MSG_DT_FIRST = _("แรก");
    var MSG_DT_LAST = _("สุดท้าย");
    var MSG_DT_NEXT = _("ถัดไป");
    var MSG_DT_PREVIOUS = _("ก่อนหน้า");
    var MSG_DT_EMPTY_TABLE = _("ไม่มีข้อมูลอยู่ในตาราง");
    var MSG_DT_INFO = _("กำลังแสดง _START_ to _END_ of _TOTAL_");
    var MSG_DT_INFO_EMPTY = _("ไม่มีรายการที่จะแสดง");
    var MSG_DT_INFO_FILTERED = _("(กรองจากรายการทั้งหมด _MAX_ รายการ)");
    var MSG_DT_LENGTH_MENU = _("แสดงรายการ _MENU_");
    var MSG_DT_LOADING_RECORDS = _("กำลังโหลด...");
    var MSG_DT_PROCESSING = _("กำลังดำเนินการ...");
    var MSG_DT_SEARCH = _("ค้นหา:");
    var MSG_DT_ZERO_RECORDS = _("ไม่พบระเบียนที่ตรงกัน");
//]]>
</script>
