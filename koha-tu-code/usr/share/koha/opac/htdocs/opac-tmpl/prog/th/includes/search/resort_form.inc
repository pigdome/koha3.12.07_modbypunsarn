[% IF sort_by == "score asc" %] <option value="score asc" selected="selected">ความสอดคล้องจากน้อยไปมาก</option>
[% ELSE %] <option value="score asc">ความสอดคล้องจากน้อยไปมาก</option>
[% END %] [% IF sort_by == "score desc" %] <option value="score desc" selected="selected">ความสอดคล้องจากมากไปน้อย</option>
[% ELSE %] <option value="score desc">ความสอดคล้องจากมากไปน้อย</option>
[% END %] [% FOREACH ind IN sortable_indexes %] [% IF sort_by == "$ind.code asc" %] <option value="[% ind.code %] asc" selected="selected">[% ind.label %] เรียงจากบนลงล่าง</option>
 [% ELSE %] <option value="[% ind.code %] asc">[% ind.label %] เรียงจากบนลงล่าง</option>
 [% END %] [% IF sort_by == "$ind.code desc" %] <option value="[% ind.code %] desc" selected="selected">[% ind.label %] เรียงจากล่างขึ้นบน</option>
 [% ELSE %] <option value="[% ind.code %] desc">[% ind.label %] เรียงจากล่างขึ้นบน</option>
 [% END %] [% END %] 