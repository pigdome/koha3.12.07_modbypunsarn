<div id="header-wrapper"><div id="members">
 [% IF ( opacuserlogin ) %] <ul>
[% UNLESS ( loggedinusername ) %] <li><a href="/cgi-bin/koha/opac-user.pl">เข้าสู่ระบบบัญชีผู้ใช้ของคุณ</a></li>[% END %] [% IF ( loggedinusername ) %] <li><span class="members">ยินดีต้อนรับ<a href="/cgi-bin/koha/opac-user.pl"><span class="loggedinusername">[% FOREACH USER_INF IN USER_INFO %][% USER_INF.title %] [% USER_INF.firstname %] [% USER_INF.surname %][% END %]</span></a></span></li>

 [% END %] [% IF ( ShowOpacRecentSearchLink ) %] <li><a href="/cgi-bin/koha/opac-search-history.pl" title="ดูประวัติการค้นหาของคุณ">ประวัติการค้นหา</a> [<a href="/cgi-bin/koha/opac-search-history.pl?action=delete" class="logout" title="ลบประวัติการค้นหาของคุณ" onclick="return confirm(MSG_DELETE_SEARCH_HISTORY);">x</a>]</li>
 [% END %] [% IF ( loggedinusername ) %]<li>[% IF persona %]<a class="logout" id="logout" href="/cgi-bin/koha/opac-main.pl?logout.x=1" onclick='navigator.id.logout();'>[% ELSE %]<a class="logout" id="logout" href="/cgi-bin/koha/opac-main.pl?logout.x=1">[% END %]ออกจากระบบ</a></li>[% END %] </ul> 
 [% END %] </div>
 [% IF ( opacheader ) %] [% opacheader %] [% END %] <div id="opac-main-search" class="yui-g">
 [% IF ( opacsmallimage ) %] <h1 id="libraryname" style="background-image: url('[% opacsmallimage %]');">
 [% ELSE %] <h1 id="libraryname">
 [% END %]<a href="/cgi-bin/koha/opac-main.pl">[% IF ( LibraryName ) %][% LibraryName %][% ELSE %]ทรัพยากรห้องสมุดออนไลน์ของ Koha[% END %]</a></h1>

<div id="fluid">

[% IF ( OpacPublic ) %] <div id="fluid-offset">
[% UNLESS ( advsearch ) %]<form name="searchform" method="get" action="/cgi-bin/koha/opac-search.pl" id="searchform">
 <label for="masthead_search" class="left"> ค้นหา [% UNLESS Koha.Preference( 'OpacAddMastheadLibraryPulldown' ) == 1 %] [% IF ( mylibraryfirst ) %] (ใน [% mylibraryfirst %] เท่านั้น)[% END %] [% END %]</label>

 <select name="idx" id="masthead_search" class="left">
 [% IF ( ms_kw ) %] <option selected="selected" value="">ทรัพยากรในห้องสมุด</option>
 [% ELSE %] <option value="">ทรัพยากรในห้องสมุด</option>
 [% END %] [% IF ( ms_ti ) %] <option selected="selected" value="ti">ชื่อเรื่อง</option>
 [% ELSE %] <option value="ti">ชื่อเรื่อง</option>
 [% END %] [% IF ( ms_au ) %] <option selected="selected" value="au">ผู้แต่ง</option>
 [% ELSE %] <option value="au">ผู้แต่ง</option>
 [% END %] [% IF ( ms_su ) %] <option selected="selected" value="su">หัวเรื่อง</option>
 [% ELSE %] <option value="su">หัวเรื่อง</option>
 [% END %] [% IF ( ms_nb ) %] <option selected="selected" value="nb">ISBN</option>
 [% ELSE %] <option value="nb">ISBN</option>
 [% END %] [% IF ( ms_se ) %] <option selected="selected" value="se">ชุด</option>
 [% ELSE %] <option value="se">ชุด</option>
 [% END %] [% IF ( numbersphr ) %] [% IF ( ms_callnum ) %] <option selected="selected" value="callnum,phr">เลขเรียกหนังสือ</option>
 [% ELSE %] <option value="callnum,phr">เลขเรียกหนังสือ</option>
 [% END %] [% ELSE %] [% IF ( ms_callnum ) %] <option selected="selected" value="callnum">เลขเรียกหนังสือ</option>
 [% ELSE %] <option value="callnum">เลขเรียกหนังสือ</option>
 [% END %] [% END %]</select>
[% IF ( ms_value ) %] <input value="[% ms_value |html %]" name="q" style="width: 35%; font-size: 111%;" type="text" title="โปรดป้อนคำค้น" id="transl1" class="left" /><div id="translControl"></div>
[% ELSE %] <input name="q" style="width: 35%; font-size: 111%;" type="text" title="โปรดป้อนคำค้น" id="transl1" class="left" /><div id="translControl"></div>
[% END %] [% IF Koha.Preference( 'OpacAddMastheadLibraryPulldown' ) == 1 %] <select name="branch_group_limit" id="select_library" class="left">
 <option value="">ทุกห้องสมุด</option>
 [% IF BranchCategoriesLoop %]<optgroup label="ห้องสมุด">[% END %] [% FOREACH BranchesLoo IN BranchesLoop %] [% IF ( BranchesLoo.selected ) %]<option selected="selected" value="branch:[% BranchesLoo.value %]">[% BranchesLoo.branchname %]</option>
 [% ELSE %]<option value="branch:[% BranchesLoo.value %]">[% BranchesLoo.branchname %]</option>[% END %] [% END %] [% IF BranchCategoriesLoop %] </optgroup>
 <optgroup label="กลุ่ม">
 [% FOREACH bc IN BranchCategoriesLoop %] [% IF ( bc.selected ) %] <option selected="selected" value="multibranchlimit-[% bc.categorycode %]">[% bc.categoryname %]</option>
 [% ELSE %] <option value="multibranchlimit-[% bc.categorycode %]">[% bc.categoryname %]</option>
 [% END %] [% END %] </optgroup>
 [% END %] </select>
 [% ELSE %] [% IF ( opac_limit_override ) %] [% IF ( opac_search_limit ) %] <input name="limit" value="[% opac_search_limit %]" type="hidden" />
 [% END %] [% ELSE %] [% IF ( mylibraryfirst ) %] <input name="limit" value="branch:[% mylibraryfirst %]" type="hidden" />
 [% END %] [% END %] [% END %] <input value="ค้นหา" type="submit" id="searchsubmit" class="left" />
[% IF ( opacbookbag ) %]<span id="cmspan"></span>[% END %] [% IF ( virtualshelves ) %]<a href="/cgi-bin/koha/opac-shelves.pl" id="listsmenulink">รายการ</a>[% END %] </form>
[% ELSE %] <div style="width:80%;*margin-bottom:-30px;">
[% IF ( virtualshelves ) %]<a href="/cgi-bin/koha/opac-shelves.pl" id="listsmenulink">รายการ</a>[% END %][% IF ( opacbookbag ) %]<span id="cmspan"></span>[% END %] </div>
[% END %] <div id="moresearches">
<a href="/cgi-bin/koha/opac-search.pl">การค้นหาขั้นสูง</a>
[% IF ( OpacBrowser ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-browser.pl">เรียกดูตามลำดับชั้น</a>[% END %] [% IF ( OpacAuthorities ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-authorities-home.pl">การค้นหาด้วยคำศัพท์ที่กำหนด</a>[% END %] [% IF ( opacuserlogin && ( Koha.Preference( 'reviewson' ) == 1 ) && ( Koha.Preference( 'OpacShowRecentComments' ) == 1 ) ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-showreviews.pl">ความคิดเห็นล่าสุด</a>[% END %] [% IF ( TagsEnabled ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-tags.pl">คลาวด์แท็ก</a>[% END %] [% IF ( OpacCloud ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-tags_subject.pl">คลาวด์หัวเรื่อง (Subject Cloud)</a>[% END %] [% IF ( OpacTopissue ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-topissues.pl">ได้รับความนิยมสูงสุด</a>[% END %] [% IF ( false ) %] [% IF ( suggestion ) %] [% IF ( AnonSuggestions ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-suggestions.pl">ข้อเสนอแนะการสั่งซื้อ</a>
 [% ELSIF ( OPACViewOthersSuggestions ) %]<span class="pipe"> | </span><a href="/cgi-bin/koha/opac-suggestions.pl">ข้อเสนอแนะการสั่งซื้อ</a>
 [% END %] [% END %] [% END %] <span class="pipe"> | </span>
<a href="http://library.tu.ac.th:8080/booksuggestion/suggestion_formt.asp" target="_blank">Book suggestion</a>
<span class="pipe"> | </span>
<a href="http://library.tu.ac.th:8080/bookdelivery/main.html" target="_blank">Book delivery</a>
</div>
 </div>
</div>

[% END %] <!-- OpacPublic --> 

[% IF ( opacbookbag ) %]<div id="cartDetails">ไม่มีรายการใดๆ ในกล่องรายการที่เลือกของคุณ</div>[% END %] [% IF ( virtualshelves ) %] <div id="listsmenu" class="yuimenu" style="display: none">
 <div class="bd">
 <h4>ชั้นหนังสือสาธารณะ</h4>
 [% IF ( pubshelves ) %] <ul class="first-of-type">
 [% FOREACH pubshelvesloo IN pubshelvesloop %] <li class="yuimenuitem"><a href="/cgi-bin/koha/opac-shelves.pl?viewshelf=[% pubshelvesloo.shelfnumber %]&amp;sortfield=[% pubshelvesloo.sortfield %]">[% pubshelvesloo.shelfname |html %]</a></li>
 [% END %] <li class="yuimenuitem"><a class="yuimenuitemlabel" href="/cgi-bin/koha/opac-shelves.pl?display=publicshelves">[ดูทั้งหมด]</a></li>
 </ul>
 [% ELSE %] ไม่มีชั้นหนังสือสาธารณะ [% END %] [% IF ( opacuserlogin ) %]<h4>ชั้นหนังสือของคุณ</h4>
 [% IF ( loggedinusername ) %] [% IF ( barshelves ) %] <ul class="first-of-type">
 [% FOREACH barshelvesloo IN barshelvesloop %] <li class="yuimenuitem"><a href="/cgi-bin/koha/opac-shelves.pl?viewshelf=[% barshelvesloo.shelfnumber %]&amp;sortfield=[% barshelvesloo.sortfield %]">[% barshelvesloo.shelfname |html %]</a></li>
 [% END %] <li class="yuimenuitem"><a class="yuimenuitemlabel" href="/cgi-bin/koha/opac-shelves.pl?display=privateshelves">[ดูทั้งหมด]</a></li>
 </ul>
 [% ELSE %] <ul class="first-of-type">
 <li>ไม่มีชั้นหนังสือส่วนตัว</li>
 <li class="yuimenuitem"><a class="yuimenuitemlabel" href="/cgi-bin/koha/opac-shelves.pl?display=privateshelves">[Create new list]</a></li></ul>
 [% END %] [% ELSE %] <ul class="first-of-type"><li><a href="/cgi-bin/koha/opac-user.pl">เข้าสู่ระบบเพื่อสร้างชั้นหนังสือของคุณเอง</a></li></ul>
 [% END %] [% END %] </div>
</div><!-- /listmenu /virtualshelves -->
[% END %]
<div id="listsDetails"></div>
</div>
</div>
<div id="breadcrumbs" class="yui-g">
[% IF ( searchdesc ) %]<p>[% IF ( total ) %]<strong>ผลการค้นหาของคุณมี [% total |html %] รายการ</strong> [% IF ( related ) %] (การค้นหาที่เกี่ยวข้อง: [% FOREACH relate IN related %][% relate.related_search %][% END %]) [% END %]<a href="[% OPACBaseURL %]/cgi-bin/koha/opac-search.pl?[% query_cgi |html |url %][% limit_cgi |html | url %]&amp;count=[% countrss |html %]&amp;sort_by=acqdate_dsc&amp;format=rss2" class="rsssearchlink"><img alt="บอกรับการค้นหานี้" src="[% interface %]/[% theme %]/images/feed-icon-16x16.png" title="บอกรับการค้นหานี้" class="rsssearchicon" /></a>
[% ELSE %] <strong>ไม่พบข้อมูลของรายการที่ค้นหา!</strong>
<p>
 [% IF ( searchdesc ) %] ไม่พบข้อมูลที่ค้นหาในรายการทรัพยากร [% LibraryName %]<a href="[% OPACBaseURL %]/cgi-bin/koha/opac-search.pl?[% query_cgi | html | url %][% limit_cgi | html | url %]&amp;format=rss2" class="rsssearchlink"><img alt="บอกรับการค้นหานี้" src="[% interface %]/[% theme %]/images/feed-icon-16x16.png" title="บอกรับการค้นหานี้" class="rsssearchicon" border="0" /></a>
 [% ELSE %] คุณไม่ได้ระบุเงื่อนไขการค้นหาใดๆ [% END %]</p>
[% IF ( OPACNoResultsFound ) %] <div id="noresultsfound">
[% OPACNoResultsFound %] </div>
[% END %] </div>
[% END %]</p>[% END %]</div>
