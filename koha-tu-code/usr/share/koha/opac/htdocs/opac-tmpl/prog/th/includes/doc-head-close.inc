</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Koha [% Version %]" /> <!-- leave this for stats -->
<link rel="shortcut icon" href="[% IF ( OpacFavicon ) %][% OpacFavicon %][% ELSE %][% themelang %]/includes/favicon.ico[% END %]" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="[% themelang %]/lib/jquery/jquery-ui.css" />
[% SET opaclayoutstylesheet='opac.css' UNLESS opaclayoutstylesheet %] [% IF (opaclayoutstylesheet.match('^https?:|^\/')) %] <link rel="stylesheet" type="text/css" href="[% opaclayoutstylesheet %]" />
[% ELSE %] <link rel="stylesheet" type="text/css" href="[% themelang %]/css/[% opaclayoutstylesheet %]" />
[% END %] [% IF ( opaccolorstylesheet ) %] [% IF (opaccolorstylesheet.match('^https?:|^\/')) %] <link rel="stylesheet" type="text/css" href="[% opaccolorstylesheet %]" />
 [% ELSE %] <link rel="stylesheet" type="text/css" href="[% themelang %]/css/[% opaccolorstylesheet %]" />
 [% END %] [% END %] [% IF ( opac_css_override ) %] <link rel="stylesheet" type="text/css" href="[% themelang %]/css/[% opac_css_override %]" />
[% END %] <link rel="stylesheet" type="text/css" media="print" href="[% themelang %]/css/print.css" />
[% IF persona %] <link rel="stylesheet" type="text/css" href="[% themelang %]/css/persona-buttons.css" />
[% END %] [% IF ( OPACMobileUserCSS ) %]<style type="text/css" media="screen and (max-width:700px)">[% OPACMobileUserCSS %]</style>[% END %] [% IF ( bidi ) %] <link rel="stylesheet" type="text/css" href="[% themelang %]/css/right-to-left.css" />
[% END %] [% IF ( OPACUserCSS ) %]<style type="text/css">[% OPACUserCSS %]</style>[% END %]
<!-- yui js --> 
<script type="text/javascript" src="[% yuipath %]/utilities/utilities.js"></script> 
<script type="text/javascript" src="[% yuipath %]/container/container-min.js"></script> 
<script type="text/javascript" src="[% yuipath %]/menu/menu-min.js"></script> 
<script type="text/javascript" src="[% themelang %]/lib/jquery/jquery.js"></script>
<script type="text/javascript" src="[% themelang %]/lib/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="[% themelang %]/lib/jquery/plugins/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript" src="[% themelang %]/js/script.js"></script>
[% IF ( OPACAmazonCoverImages ) %] <script type="text/javascript" language="javascript">//<![CDATA[
		var NO_AMAZON_IMAGE = _("ไม่มีภาพปก");
	//]]>
</script>
<script type="text/javascript" language="javascript" src="[% themelang %]/js/amazonimages.js"></script>
[% END %] [% IF ( SyndeticsCoverImages ) %] <script type="text/javascript">//<![CDATA[
        var NO_AMAZON_IMAGE = _("ไม่มีภาพปก");
    //]]>
</script>
<script type="text/javascript" src="[% themelang %]/js/amazonimages.js"></script> [% END %] [% IF ( opacbookbag ) %]<script type="text/javascript" src="[% themelang %]/js/basket.js">
[% ELSIF ( virtualshelves ) %]<script type="text/javascript" src="[% themelang %]/js/basket.js">
[% ELSE %]<script type="text/javascript"> var readCookie;[% END %]
</script>

<script type="text/javascript">
	//<![CDATA[
	var MSG_CONFIRM_AGAIN = _("คำเตือน: ไม่สามารถยกเลิกได้ โปรดยืนยันอีกครั้ง")
    var MSG_DELETE_SEARCH_HISTORY = _("คุณแน่ใจหรือไม่ว่า คุณต้องการลบประวัติการค้นหาของคุณ");
    [% IF ( opacbookbag ) %]var MSG_BASKET_EMPTY = _("ขณะนี้ ไม่มีรายการใดๆ ในกล่องรายการของคุณ");
    var MSG_RECORD_IN_BASKET = _("มีรายการนี้อยู่ในกล่องรายการที่เลือกของคุณแล้ว");
    var MSG_RECORD_ADDED = _("คุณได้ใส่รายการนี้ลงไปในกล่องรายการที่เลือกของคุณแล้ว");
    var MSG_RECORD_REMOVED = _("รายการนี้ถูกเอาออกจากรถเข็นหนังสือของคุณแล้ว");
    var MSG_NRECORDS_ADDED = _(" รายการที่เพิ่มในรถเข็นหนังสือของคุณ");
    var MSG_NRECORDS_IN_BASKET = _("อยู่ในรายการที่เลือกของคุณแล้ว");
    var MSG_NO_RECORD_SELECTED = _("คุณไม่ได้เลือกรายการใดๆ");
    var MSG_NO_RECORD_ADDED = _("ไม่มีการเพิ่มรายการใดๆ ลงในกล่องรายการที่เลือก");
    var MSG_CONFIRM_DEL_BASKET = _("คุณแน่ใจหรือไม่ว่า คุณต้องการลบรายการในรถเข็นหนังสือออก");
    var MSG_CONFIRM_DEL_RECORDS = _("คุณแน่ใจหรือไม่ว่า คุณต้องการเอารายการที่เลือกออก");
    var MSG_ITEM_IN_CART = _("ในรถเข็นหนังสือของคุณ");
    var MSG_IN_YOUR_CART = _("รายการในรถเข็นของคุณ:");
    var MSG_ITEM_NOT_IN_CART = _("เพิ่มไปยังรถเข็นหนังสือของคุณ");
    $("#cartDetails").ready(function(){ $("#cmspan").html("<a href=\"#\" id=\"cartmenulink\"><span id=\"carticon\"></span> "+_("รถเข็นหนังสือ")+"<span id=\"basketcount\"><\/span><\/a>"); }); [% ELSE %][% IF ( virtualshelves ) %]
    var MSG_NO_RECORD_SELECTED = _("คุณไม่ได้เลือกรายการใดๆ");[% END %][% END %]
    [% IF ( opacuserlogin ) %][% IF ( TagsEnabled ) %]var MSG_TAGS_DISABLED = _("ขออภัย! ระบบนี้ไม่ได้เปิดใช้งานแท็ก");
    var MSG_TAG_ALL_BAD = _("ข้อผิดพลาด! แท็กของคุณเป็นมาร์กอัพโค้ดทั้งหมด ดังนั้นจึงไม่สามารถเพิ่มได้ โปรดลองอีกครั้งด้วยข้อความล้วน");
    var MSG_ILLEGAL_PARAMETER = _("ข้อผิดพลาด! พารามิเตอร์ไม่ถูกต้อง");
    var MSG_TAG_SCRUBBED = _("หมายเหตุ: แท็กของคุณมีมาร์กอัพโค้ดที่ถูกลบออกไปแล้ว โดยมีการเพิ่มแท็กเป็น");
    var MSG_ADD_TAG_FAILED = _("ข้อผิดพลาด! การดำเนินการ add_tag ล้มเหลวที่");
    var MSG_ADD_TAG_FAILED_NOTE = _("หมายเหตุ: คุณสามารถแท็กรายการด้วยคำที่กำหนดไว้ได้ครั้งเดียวเท่านั้น เลือก 'แท็กของฉัน' เพื่อดูแท็กปัจจุบันของคุณ");
    var MSG_DELETE_TAG_FAILED = _("ข้อผิดพลาด! คุณไม่สามารถลบแท็กได้");
    var MSG_DELETE_TAG_FAILED_NOTE = _("หมายเหตุ: คุณสามารถลบเฉพาะแท็กที่คุณสร้างได้เท่านั้น")
    var MSG_LOGIN_REQUIRED = _("คุณต้องเข้าสู่ระบบเพื่อเพิ่มแท็ก");
    var MSG_TAGS_ADDED = _("แท็กที่เพิ่ม:");
    var MSG_TAGS_DELETED = _("แท็กที่เพิ่ม:");
    var MSG_TAGS_ERRORS = _("ข้อผิดพลาด:");
    var MSG_MULTI_ADD_TAG_FAILED = _("ไม่สามารถเพิ่มแท็กได้อย่างน้อยหนึ่งแท็ก");
    var MSG_NO_TAG_SPECIFIED = _("ไม่มีการระบุแท็ก");[% END %][% END %]
	[% IF ( OPACAmazonCoverImages ) %]$(window).load(function() {
		 	verify_images();
		 });[% END %]
	[% IF ( SyndeticsCoverImages ) %]$(window).load(function() {
            verify_images();
         });[% END %]
	[% IF ( opacuserjs ) %][% opacuserjs %][% END %]
	//]]>
[% IF ( opacbookbag ) %]</script><script type="text/javascript" src="[% themelang %]/js/basket.js">
[% ELSIF ( virtualshelves ) %]</script><script type="text/javascript" src="[% themelang %]/js/basket.js">
[% ELSE %]</script><script type="text/javascript">var readCookie;[% END %]</script>
[% IF ( opacuserlogin ) %][% IF ( TagsEnabled ) %]<script type="text/javascript" src="[% themelang %]/js/tags.js"></script>[% END %][% ELSE %][% END %] [% IF ( GoogleJackets ) %] <script type="text/javascript" src="[% themelang %]/js/google-jackets.js"></script>
<script type="text/javascript">
	//<![CDATA[
	var NO_GOOGLE_JACKET = _("ไม่มีภาพปก");
	//]]>
</script>
[% END %] [% IF OpenLibraryCovers %] <script type="text/javascript" src="[% themelang %]/js/openlibrary.js"></script>
<script type="text/javascript">
//<![CDATA[
var NO_OL_JACKET = _("ไม่มีภาพปก");
//]]>
</script>
[% END %] [% IF OPACLocalCoverImages %] <script type="text/javascript" src="[% themelang %]/js/localcovers.js"></script>
<script type="text/javascript">
//<![CDATA[
var NO_LOCAL_JACKET = _("ไม่มีภาพปก");
//]]>
</script>
[% END %] [% IF ( BakerTaylorEnabled ) %]<script type="text/javascript" src="[% themelang %]/js/bakertaylorimages.js"></script>
<script type="text/javascript">
	//<![CDATA[
	var NO_BAKERTAYLOR_IMAGE = _("ไม่มีภาพปก");
	$(window).load(function(){
		bt_verify_images();
	});
	//]]>
</script>[% END %] <link rel="unapi-server" type="application/xml" title="unAPI" href="[% OPACBaseURL %]/cgi-bin/koha/unapi" />
[% IF ( GoogleIndicTransliteration ) %] <script type="text/javascript" src="http://www.google.com/jsapi"></script>
 <script type="text/javascript" src="[% themelang %]/js/googleindictransliteration.js"></script>
[% END %] 