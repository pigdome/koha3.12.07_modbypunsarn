/*!40000 ALTER TABLE `quotes` DISABLE KEYS */;
INSERT INTO `quotes` VALUES
(1,'Muriac','La paura è il principio della saggezza..','0000-00-00 00:00:00'),
(2,'Giordano','Come ci si sente stupidi a pensare a tutto il tempo che sprechiamo a desiderare di essere altrove.','0000-00-00 00:00:00'),
(3,'Bach', 'Nessun luogo è lontano.','0000-00-00 00:00:00'),
(4,'Disney','Per rendere una cosa speciale, devi solo credere che sia speciale non esiste un ingrediente segreto.','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `quotes` ENABLE KEYS */;
