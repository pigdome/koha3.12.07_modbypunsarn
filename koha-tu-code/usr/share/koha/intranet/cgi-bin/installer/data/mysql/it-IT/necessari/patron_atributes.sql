INSERT INTO `borrower_attribute_types` (`code`, `description`, `repeatable`, `unique_id`, `opac_display`, `password_allowed`, `staff_searchable`, `authorised_value_category`)
VALUES ('SHOW_BCODE',  'Mostra il barcode dell\'utente nella schermata della lista delle copie', 0, 0, 1, 0, 0, 'YES_NO');
