--- 
OPAC: 
  Appearance: 
    - 
      - Use the
      - 
        choices: opac-templates
        pref: opacthemes
      - theme on the OPAC.
    - 
      - The OPAC is located at http://
      - 
        class: url
        pref: OPACBaseURL
      - . Do not include a trailing slash in the URL. (This must be filled in correctly for RSS, unAPI, and search plugins to work.)
    - 
      - Show
      - 
        class: long
        pref: LibraryName
      - as the name of the library on the OPAC.
    - 
      - 
        choices: 
          "": Disable
          1: Enable
        pref: OpacPublic
      - Koha OPAC as public. Private OPAC requires authentification before accessing the OPAC.
    - 
      - 
        choices: 
          "": Don't emphasize
          1: Emphasize
        pref: HighlightOwnItemsOnOPAC
      - "results from the "
      - 
        choices: 
          OpacURLBranch: OPAC's branch via the URL
          PatronBranch: patron's home branch
        pref: HighlightOwnItemsOnOPACWhich
      - " by moving the results to the front and increasing the size or highlighting the rows for those results. (Non-XSLT Only)"
    - 
      - Show star-ratings on
      - 
        choices: 
          all: results and details
          details: only details
          disable: 'no'
        pref: OpacStarRatings
      - pages.
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: OpacMaintenance
      - "a warning that the OPAC is under maintenance, instead of the OPAC itself. Note: this shows the same warning as when the database needs to be upgraded, but unconditionally."
    - 
      - By default, show bib records
      - 
        choices: 
          isbd: as specified in the ISBD template.
          marc: in their MARC form.
          normal: in simple form.
        pref: BiblioDefaultView
    - 
      - When patrons click on a link to another website from your OPAC (like Amazon or OCLC),
      - 
        choices: 
          "": don't
          1: do
        pref: OPACURLOpenInNewWindow
      - open the website in a new window.
    - 
      - 
        choices: 
          "": Show
          1: Don't show
        pref: hidelostitems
      - lost items on search and detail pages.
    - 
      - "Display OPAC results using XSLT stylesheet at: "
      - 
        class: file
        pref: OPACXSLTResultsDisplay
      - <br />Options:<ul><li><a href="#" class="set_syspref" data-syspref="OPACXSLTResultsDisplay" data-value="">Leave empty</a> for "no xslt"</li><li>enter "<a href="#" class="set_syspref" data-syspref="OPACXSLTResultsDisplay" data-value="default">default</a>" for the default one</li><li> put a path to define a xslt file</li><li>put an URL for an external specific stylesheet.</li></ul>{langcode} will be replaced with current interface language
    - 
      - "Display OPAC details using XSLT stylesheet at: "
      - 
        class: file
        pref: OPACXSLTDetailsDisplay
      - <br />Options:<ul><li><a href="#" class="set_syspref" data-syspref="OPACXSLTDetailsDisplay" data-value="">Leave empty</a> for "no xslt"</li><li>enter "<a href="#" class="set_syspref" data-syspref="OPACXSLTDetailsDisplay" data-value="default">default</a>" for the default one</li><li>put a path to define a xslt file</li><li>put an URL for an external specific stylesheet.</li></ul>{langcode} will be replaced with current interface language
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: DisplayOPACiconsXSLT
      - the format, audience, and material type icons in XSLT MARC21 results and detail pages in the OPAC.
    - 
      - 
        choices: 
          "": Don't include
          1: Include
        pref: COinSinOPACResults
      - "COinS / OpenURL / Z39.88 in OPAC search results.  <br/>Warning: Enabling this feature will slow OPAC search response times."
    - 
      - 
        choices: 
          "": Do not show
          1: Show
        pref: OPACShowUnusedAuthorities
      - unused authorities in the OPAC authority browser.
    - 
      - 
        choices: 
          holds: Show holds
          holds_priority: Show holds and their priority level
          none: Don't show any hold details
          priority: Show priority level
        pref: OPACShowHoldQueueDetails
      - to patrons in the OPAC.
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        default: 0
        pref: OPACShowCheckoutName
      - the name of the patron that has an item checked out on item detail pages on the OPAC.
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        default: 0
        pref: OpacKohaUrl
      - "'Powered by Koha' text on OPAC footer."
    - 
      - 
        choices: 
          "": Zeige einen
          1: Zeige keinen
        default: 0
        pref: OpacShowRecentComments
      - "Link zu k\xC3\xBCrzlich verfassten Kommentaren in der Navigationsleiste des OPACs."
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        default: 0
        pref: OPACShowBarcode
      - the item's barcode on the holdings tab.
    - 
      - 
        choices: 
          "": Don't highlight
          1: Highlight
        pref: OpacHighlightedWords
      - words the patron searched for in their search results and detail pages.
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: AuthorisedValueImages
      - images for <a href="/cgi-bin/koha/admin/authorised_values.pl">authorized values</a> (such as lost statuses and locations) in search results and item detail pages on the OPAC.
    - 
      - Use the image at
      - 
        class: url
        pref: opacsmallimage
      - in the OPAC header, instead of the Koha logo. If this image is a different size than the Koha logo, you will need to customize the CSS. (This should be a complete URL, starting with <code>http://</code>.)
    - 
      - Use the image at
      - 
        class: url
        pref: OpacFavicon
      - for the OPAC's favicon. (This should be a complete URL, starting with <code>http://</code>.)
    - 
      - "Include the following JavaScript on all pages in the OPAC:"
      - 
        class: code
        pref: opacuserjs
        type: textarea
    - 
      - Include the additional CSS stylesheet
      - 
        class: file
        pref: opaccolorstylesheet
      - to override specified settings from the default stylesheet (leave blank to disable). Enter just a filename, a full local path or a complete URL starting with <code>http://</code> (if the file lives on a remote server). Please note that if you just enter a filename, the file should be in the css subdirectory for each active theme and language within the Koha templates directory. A full local path is expected to start from your HTTP document root.
    - 
      - "opac.pref#opaclayoutstylesheet# Utiliser la feuille de style CSS "
      - 
        class: file
        pref: opaclayoutstylesheet
      - on all pages in the OPAC, instead of the default css (used when leaving this field blank). Enter just a filename, a full local path or a complete URL starting with <code>http://</code> (if the file lives on a remote server). Please note that if you just enter a filename, the file should be in the css subdirectory for each active theme and language within the Koha templates directory. A full local path is expected to start from your HTTP document root.
    - 
      - "Include the following CSS on all pages in the OPAC:"
      - 
        class: code
        pref: OPACUserCSS
        type: textarea
    - 
      - "Include the following CSS for the mobile view on all pages in the OPAC:"
      - 
        class: code
        pref: OPACMobileUserCSS
        type: textarea
    - 
      - "Show the following HTML in its own column on the main page of the OPAC:"
      - 
        class: code
        pref: OpacMainUserBlock
        type: textarea
    - 
      - "Show the following HTML in its own column on the main page of the OPAC (mobile version):"
      - 
        class: code
        pref: OpacMainUserBlockMobile
        type: textarea
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: OpacShowLibrariesPulldownMobile
      - the libraries pulldown on the mobile version of the OPAC.
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: OpacShowFiltersPulldownMobile
      - the search filters pulldown on the mobile version of the OPAC.
    - 
      - "Show the following HTML on the left hand column of the main page and patron account on the OPAC (generally navigation links):"
      - 
        class: code
        pref: OpacNav
        type: textarea
    - 
      - "Show the following HTML in the right hand column of the main page under the main login form:"
      - 
        class: code
        pref: OpacNavRight
        type: textarea
    - 
      - "Zeige das folgende HTML in der linken Spalte der Startseite und im Benutzerkonto oberhalb von OpacNav und vor vorhandenen Navigationsreitern an:"
      - 
        class: code
        pref: OpacNavBottom
        type: textarea
    - 
      - "Include the following HTML in the header of all pages in the OPAC:"
      - 
        class: code
        pref: opacheader
        type: textarea
    - 
      - "Include the following HTML in the footer of all pages in the OPAC:"
      - 
        class: code
        pref: opaccredits
        type: textarea
    - 
      - "Include a \"More Searches\" box on the detail pages of items on the OPAC, with the following HTML (leave blank to disable):"
      - "opac.pref#OPACSearchForTitleIn# <br />Note: Les param\xC3\xA8tres {BIBLIONUMBER}, {CONTROLNUMBER}, {TITLE}, {ISBN}, {ISSN} and {AUTHOR} seront remplac\xC3\xA9s par les informations de la notice affich\xC3\xA9e."
      - 
        class: code
        pref: OPACSearchForTitleIn
        type: textarea
    - 
      - "Include a \"Links\" column on the \"my summary\" and \"my reading history\" tabs when a user is logged in to the OPAC, with the following HTML (leave blank to disable):"
      - "<br />Note: The placeholders {BIBLIONUMBER}, {TITLE}, {ISBN} and {AUTHOR} will be replaced with information from the displayed record."
      - 
        class: code
        pref: OPACMySummaryHTML
        type: textarea
    - 
      - "Include the following HTML under the facets in OPAC search results:"
      - 
        class: code
        pref: OPACResultsSidebar
        type: textarea
    - 
      - 
        choices: 
          "": Don't add
          1: Add
        pref: OpacAddMastheadLibraryPulldown
      - a library select pulldown menu on the OPAC masthead.
    - 
      - "Display this HTML when no results are found for a search in the OPAC:"
      - "<br />Note: You can insert placeholders {QUERY_KW} that will be replaced with the keywords of the query."
      - 
        class: code
        pref: OPACNoResultsFound
        type: textarea
    - 
      - "Display the URI in the 856u field as an image on: "
      - 
        choices: 
          "": Neither Details or Results pages
          Both: Both Details and Results pages
          Details: Details page only
          Results: Results page only
        pref: OPACDisplay856uAsImage
      - "Note: The corresponding OPACXSLT option must be turned on."
    - 
      - "opac.pref#OpacExportOptions# Lister les options d'export qui seront disponibles depuis l'affichage d\xC3\xA9taill\xC3\xA9e de l'OPAC, en les s\xC3\xA9parant par |:"
      - 
        class: multi
        pref: OpacExportOptions
      - "opac.pref#OpacExportOptions# <br />Note: les options disponibles sont: BIBTEX (<code>bibtex</code>), Dublin Core (<code>dc</code>),"
      - MARCXML (<code>marcxml</code>), MARC-8 encoded MARC (<code>marc8</code>), Unicode/UTF-8 encoded MARC (<code>utf8</code>),
      - opac.pref#OpacExportOptions# Unicode/UTF-8 encoded MARC without local use -9xx, x9x, xx9- fields and subfields (<code>marcstd</code>), MODS (<code>mods</code>), RIS (<code>ris</code>)
    - 
      - 
        choices: 
          "": Don't separate
          1: Separate
        pref: OpacSeparateHoldings
      - items display into two tabs, where the first tab contains items whose
      - 
        choices: 
          holdingbranch: holding library
          homebranch: home library
        pref: OpacSeparateHoldingsBranch
      - is the logged in user's library. The second tab will contain all other items.
  Features: 
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: opacuserlogin
      - patrons to log in to their accounts on the OPAC.
    - 
      - Show
      - 
        choices: 
          callnum: call number only
          ccode: collection code
          location: location
        pref: OpacItemLocation
      - for items on the OPAC search results.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OpacPasswordChange
      - patrons to change their own password on the OPAC. Note that this must be off to use LDAP authentication.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OPACPatronDetails
      - patrons to notify the library of changes to their contact information from the OPAC.
    - 
      - 
        choices: 
          "": opac.pref#OPACpatronimages# Ne pas afficher
          1: opac.pref#OPACpatronimages# Afficher
        pref: OPACpatronimages
      - "opac.pref#OPACpatronimages# les photos des adh\xC3\xA9rents sur la page des informations personnelles \xC3\xA0 l'OPAC."
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OPACFinesTab
      - patrons to access the Fines tab on the My Account page on the OPAC.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OpacBrowser
      - patrons to browse subject authorities on OPAC (run misc/cronjobs/build_browser_and_cloud.pl to create the browser list)
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: OpacCloud
      - a subject cloud on OPAC (run misc/cronjobs/build_browser_and_cloud.pl to build)
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OpacAuthorities
      - patrons to search your authority records.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: opacbookbag
      - patrons to store items in a temporary "Cart" on the OPAC.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OpacTopissue
      - patrons to access a list of the most checked out items on the OPAC. Note that this is somewhat experimental, and should be avoided if your collection has a large number of items.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: suggestion
      - patrons to make purchase suggestions on the OPAC.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: reviewson
      - patrons to make comments on items on the OPAC.
    - 
      - Show
      - 
        choices: 
          first: "opac.pref#ShowReviewer# pr\xC3\xA9nom"
          firstandinitial: "opac.pref#ShowReviewer# pr\xC3\xA9nom et initial du nom de famille"
          full: opac.pref#ShowReviewer# nom entier
          none: opac.pref#ShowReviewer# aucun nom
          surname: opac.pref#ShowReviewer# nom de famille
          username: opac.pref#ShowReviewer# identifiant
        pref: ShowReviewer
      - "opac.pref#ShowReviewer# de l'auteur avec ses commentaires \xC3\xA0 l'OPAC."
    - 
      - 
        choices: 
          "": Hide
          1: Show
        pref: ShowReviewerPhoto
      - reviewer's photo beside comments in OPAC.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: RequestOnOpac
      - patrons to place holds on items from the OPAC.
    - 
      - Display
      - 
        class: long
        pref: numSearchRSSResults
      - search results in the RSS feed.
    - 
      - 
        choices: 
          "": Disable
          1: Enable
        default: 0
        pref: SocialNetworks
      - social network links in opac detail pages
    - 
      - 
        choices: 
          "": Disable
          1: Enable
        default: 1
        pref: OpacBrowseResults
      - browsing and paging search results from the OPAC detail page.
    - 
      - 
        choices: 
          "": Disable
          1: Enable
        default: 0
        pref: QuoteOfTheDay
      - Quote of the Day display on OPAC home page
    - 
      - 
        choices: 
          "": Don't display
          1: Display
        default: 0
        pref: OPACPopupAuthorsSearch
      - the list of authors/subjects in a popup for a combined search on OPAC detail pages.
  Policy: 
    - 
      - 
        choices: 
          "": Allow
          1: Don't allow
        default: 0
        pref: singleBranchMode
      - patrons to select their branch on the OPAC or show branch names with callnumbers.
    - 
      - 
        choices: 
          "": Don't limit
          1: Limit
        pref: SearchMyLibraryFirst
      - patrons' searches to the library they are registered at.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OPACItemHolds
      - patrons to place holds on specific items in the OPAC. If this is disabled, users can only put a hold on the next available item.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: OpacRenewalAllowed
      - patrons to renew their own books on the OPAC.
    - 
      - Use
      - 
        choices: 
          "": 'NULL'
          checkoutbranch: the library the item was checked out from
          itemhomebranch: the item's home library
          opacrenew: "'OPACRenew'"
          patronhomebranch: the patron's home library
        pref: OpacRenewalBranch
      - as branchcode to store in the statistics table.
    - 
      - Only allow patrons to renew their own books on the OPAC if they have less than
      - 
        class: currency
        pref: OPACFineNoRenewals
      - "[% local_currency %] in fines (leave blank to disable)."
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: OPACViewOthersSuggestions
      - purchase suggestions from other patrons on the OPAC.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        default: 0
        pref: AllowPurchaseSuggestionBranchChoice
      - patrons to select library when making a purchase suggestion
    - 
      - 
        class: code
        pref: OpacHiddenItems
        type: textarea
      - Allows to define custom rules for hiding specific items at opac. See docs/opac/OpacHiddenItems.txt for more informations
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        default: 1
        pref: OpacAllowPublicListCreation
      - opac users to create public lists
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        default: 0
        pref: OpacAllowSharingPrivateLists
      - opac users to share private lists with other patrons. This feature is not active yet but will be released soon
  Privacy: 
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: AnonSuggestions
      - patrons that aren't logged in to make purchase suggestions. Suggestions are connected to the AnonymousPatron syspref
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: opacreadinghistory
      - patrons to see what books they have checked out in the past.
    - 
      - 
        choices: 
          "": Don't keep
          1: Keep
        default: 0
        pref: EnableOpacSearchHistory
      - patron search history in the OPAC.
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        default: 0
        pref: OPACPrivacy
      - patrons to choose their own privacy settings for their reading history.  This requires opacreadinghistory and AnonymousPatron
    - 
      - Use borrowernumber
      - 
        class: integer
        pref: AnonymousPatron
      - as the Anonymous Patron (for anonymous suggestions and reading history)
    - 
      - 
        choices: 
          "": Don't track
          anonymous: Track anonymously
          track: Track
        default: 0
        pref: TrackClicks
      - links that patrons click on
  Self Registration: 
    - 
      - 
        choices: 
          "": Don't allow
          1: Allow
        pref: PatronSelfRegistration
      - library patrons to register an account via the OPAC.
    - 
      - 
        choices: 
          "": Don't require
          1: Require
        pref: PatronSelfRegistrationVerifyByEmail
      - that a self-registering patron verify his or herself via email.
    - 
      - Use the patron category code
      - 
        class: short
        pref: PatronSelfRegistrationDefaultCategory
      - as the default patron category for patrons registered via the OPAC.
    - 
      - Delete patrons registered via the OPAC, but not yet verified after
      - 
        class: integer
        pref: PatronSelfRegistrationExpireTemporaryAccountsDelay
      - days.
    - 
      - "The following <a href='http://schema.koha-community.org/tables/borrowers.html' target='blank'>database columns</a> must be filled in on the patron entry screen:"
      - 
        class: multi
        pref: PatronSelfRegistrationBorrowerMandatoryField
      - (separate columns with |)
    - 
      - "The following <a href='http://schema.koha-community.org/tables/borrowers.html' target='blank'>database columns</a> will not appear on the patron entry screen:"
      - 
        class: multi
        pref: PatronSelfRegistrationBorrowerUnwantedField
      - (separate columns with |)
    - 
      - "Display the following additional instructions for patrons who self register via the OPAC ( HTML is allowed ):"
      - 
        class: html
        pref: PatronSelfRegistrationAdditionalInstructions
        type: textarea
  Shelf Browser: 
    - 
      - 
        choices: 
          "": Don't show
          1: Show
        pref: OPACShelfBrowser
      - a shelf browser on item details pages, allowing patrons to see what's near that item on the shelf. Note that this uses up a fairly large amount of resources on your server, and should be avoided if your collection has a large number of items.
    - 
      - 
        choices: 
          "": Don't use
          1: Use
        default: 1
        pref: ShelfBrowserUsesLocation
      - the item location when finding items for the shelf browser.
    - 
      - 
        choices: 
          "": Don't use
          1: Use
        default: 1
        pref: ShelfBrowserUsesHomeBranch
      - the item home library when finding items for the shelf browser.
    - 
      - 
        choices: 
          "": Don't use
          1: Use
        default: 0
        pref: ShelfBrowserUsesCcode
      - "\xD1\x88\xD0\xB8\xD1\x84\xD1\x80 \xD1\x81\xD0\xBE\xD0\xB1\xD1\x80\xD0\xB0\xD0\xBD\xD0\xB8\xD1\x8F"
