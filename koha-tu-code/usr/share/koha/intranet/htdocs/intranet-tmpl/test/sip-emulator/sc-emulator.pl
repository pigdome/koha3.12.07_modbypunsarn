#!/usr/bin/perl

use warnings;
use strict;

our $user     = 'pigdome';
our $password = 'pigdome55';
our $patron   = '111222444666';
our $barcode  = '31379013318713';
our $loc      = 'TUPUEY';

require 'config.pl' if -e 'config.pl';

use lib 'lib';
use SIP2::SC;

#my $sc = SIP2::SC->new( $ENV{ACS} || '127.0.0.1:6002' );
my $sc = SIP2::SC->new( $ENV{ACS} || '127.0.0.1:6442' );

# login
#$sc->message("9300CN$user|CO$password|");

# SC Status
#$sc->message("9900302.00");

#$sc->message("6300020091214    085452          AO$loc|AA$patron|AC$password|");

# Checkout
$sc->message("11YN20091214    124436                  AO$loc|AA$patron|AB$barcode|AC$password|BON|BIN|");

# Checkin
#$sc->message("09N20091214    08142820091214    081428AP$loc|AO$loc|AB$barcode|AC|BIN|");

#$sc->message("09N20150122    03234720150122    032347APTU|AOTU|AB31379014123757|AC|");

# checkout - invalid barcode
#$sc->message("09N20091216    15320820091216    153208AP|AOFFZG|AB200903160190|ACviva2koha|BIN|");

# status
#$sc->message("9900302.00");

