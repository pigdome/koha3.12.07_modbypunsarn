var PrintTools = function(){};

    
        
    PrintTools.libraryLocation = "";

	PrintTools.newTab = function(condition,lib_input)
	{
		console.log(condition);
        var list;
		if(condition=="build") {
		 list=16;
	    }
		else if(condition=="update") {
			list=document.getElementById('list').value;
		}

		var today=new Date();
		var h=today.getHours();
		var mi=today.getMinutes();
		var s=today.getSeconds();
		var y=today.getFullYear()+543;
		var m=today.getMonth()+1;
		var d=today.getDate();
		var w=today.getDay();
		h = PrintTools.checkTime(h);
		mi = PrintTools.checkTime(mi);
		s = PrintTools.checkTime(s);
		m = PrintTools.checkTime(m);
		d = PrintTools.checkTime(d);
		var week;

		if(w==0){
			week = "อาทิตย์";
		}else if(w==1){
			week = "จันทร์";
		}else if(w==2){
			week = "อังคาร";                   
		}else if(w==3){
			week = "พุธ";                       
		}else if(w==4){
			week = "พฤหัสบดี";                        
		}else if(w==5){
			week = "ศุกร์";                
		}else if(w==6){
			week = "เสาร์";              
		}

		var lib;
		if(condition=="build") lib=PrintTools.libraryLocation.trim();
		else if(condition=="update") lib=lib_input;

		var front = "<div style='page-break-after:always'>"+
		"<h3 style='text-align:center'>หนังสือ Book Delivery มาถึง "+lib+" วัน "+week+" ที่ "+d+"/"+m+"/"+y+"    "+h+":"+mi+":"+s+"น.</h3>"+
		"<center>"+
		"<table>"+
		"<thead>"+
		"<tr align='center'>"+
		"<th style='width:3.1%; max-width:3.1%'></th>"+
		"<th style='word-break:break-all;width:22.1%;'>Name</th>"+
		"<th style='word-break:break-all;width:17.5%;'>Call.No.</th>"+
		"<th style='width:6%; max-width:6%'>Copy</th>"+
		"<th style='word-break:break-all;width:12.7%'>Barcode</th>"+
		"<th style='width:16%; max-width:16%'>Title</th>"+
		"<th style='width:3.6%; max-width:3.6%'>Loc.</th>"+
		"<th style='width:10.7%; max-width:10.7%; font-size:13px'>ชื่อผู้รับหนังสือ/วันที่</th>"+
		"<th style='width:8.3%; max-width:8.3%; font-size:14px'>เจ้าหน้าที่</th>"+
		"</tr>"+
		"</thead>"+
		"<tbody>";

		var end =     "</tbody>"+
		"</table>"+
		"</center>"+
		"</div>";
		var data ="<html>"+
		"<head>"+
		"<meta name = 'viewport' content = 'width=device-width,initial-scale=1.0' charset='UTF-8'>"+
		"<script src='js/jquery-2.1.1.min.js'></script>"+
		"<script src='js/app/authentication/tools.js'></script>"+
	    "<script src='js/app/bookdelivery/tools.js'></script>"+
	    "<script src='js/app/authentication/tools.js'></script>"+
	    "<script src='js/app/export-to-text/tools.js'></script>"+
	    "<script src='js/app/print/tools.js'></script>"+
	    "<script src='js/app/table/tools.js'></script>"+
		"<style>"+
		"table,th,td{"+
		"border:1px solid black;"+
		"border-collapse:collapse;"+
		"margin-top: 10px;"+
		"padding-top: 10px;"+
		"width: 100%;"+
		"max-width: 100%;"+
		"break-word: word-wrap;"+
		"table-layout:fixed;"+
		"}"+
		"th,td{"+
		"padding:5px;"+
		"display: table-cell;"+
		"break-word: word-wrap;"+
		"}"+
		"tr{"+
		"height: 15px;"+
		"}"+
		"</style>"+
		"</head>"+
		"<title>Book Delivery Sign Form</title>"+
		"<body>"+
		"<form>"+
		"จำนวนข้อมูล/หน้า"+
		"<input type='text' class='searchtextbox' id='list' value='"+list+"'>"+
		"<button onclick='PrintTools.newTab(\"update\",\""+lib+"\")'>Update Page</button>"+
		"</form>"+
		"<div id='print'>"+
		front;

		var j ;

		var count;
		if(condition=="build") count =  document.getElementsByTagName('td').length/8;
		else if(condition=="update") count =  document.getElementsByTagName('td').length/9;

		for(var i = 0;i<count;i++)
		{    
			if(condition=="build") j = i*8;
			else if(condition=="update") j = i*9;
			data += "<tr align='left' padding='5px'>"+
			"<td style='width:3.1%; max-width:3.1%'>"+document.getElementsByTagName('td')[j++].innerHTML+"</td>"+
			"<td style='word-break:break-all;width:22.1%'>"+document.getElementsByTagName('td')[j++].innerHTML+"</td>"+
			"<td style='word-break:break-all;width:17.5%'>"+document.getElementsByTagName('td')[j++].innerHTML+"</td>"+
			"<td style='width:6%; max-width:6%'>"+document.getElementsByTagName('td')[j++].innerHTML+"</td>"+
			"<td style='word-break:break-all;width:12.7%'>"+document.getElementsByTagName('td')[j++].innerHTML+"</td>"+
			"<td style='width:16%; max-width:16%; overflow:hidden; text-overflow: ellipsis; white-space: nowrap;'>"+document.getElementsByTagName('td')[j++].innerHTML+"</td>"+
			"<td style='width:3.6%; max-width:3.6%; overflow:hidden; text-overflow: ellipsis; white-space: nowrap;'>"+document.getElementsByTagName('td')[j++].innerHTML+"</td>"+
			"<td></td>"+
			"<td></td>"+
			"</tr>";
			if(((i+1)%list==0)&&(i!=count-1)){
				data += end + front;
			}
		}
		data += end +
		"</div>"+
		"</br>"+
		"<button onclick='PrintTools.print()'>Print</button>"+
		"</body>"+
		"</html>";
        
		if(condition=="build"){
			var myWindow = window.open("",'_blank'); 
			myWindow.document.write(data);
		}
		else if(condition=="update") {
		    var myWindow = window.open("",'_blank'); 
			myWindow.document.write(data);
			window.close();
		}
	}

	PrintTools.print = function(){

	    var originalContents = document.body.innerHTML;
		var printContents = document.getElementById('print').innerHTML;   
		document.body.innerHTML = printContents;
		window.print();
		location.reload();
		document.body.innerHTML = originalContents;
	};

	PrintTools.checkTime = function(i){
        if (i<10) {i = "0" + i};
		   return i;
	};

	PrintTools.init = function(){
         $('#btn-print').on("click", function(event) {
            event.preventDefault();
			PrintTools.newTab("build","");
		});
	};
	
