var BookDeliveryTools = function(argument) {

};
	

	BookDeliveryTools.getInfoFromBarcode = function(){
		if(ValidateTools.checkBarcode())
          BookDeliveryConnect.get();
	};

	BookDeliveryTools.extractDataFromHtml = function(data){

        var tempHtml = $( '<div></div>' );
		tempHtml.html(data); // Create temp html for query

		var title = $("div#breadcrumbs i",tempHtml).text();
		var libraryLocation = $('a.dropdown-toggle strong',tempHtml).text();
        
        // get row <tr> where barcode == barcode
		var row = ($("a[href*='/cgi-bin/koha/catalogue/moredetail.pl']", tempHtml).filter(function()
		{return $(this).text() == $("#barcode").val();}).parent().parent());

		tempHtml = $( '<div></div>' );
		tempHtml.html(row);

		var location = BookDeliveryTools.mapLocation($("td.location",tempHtml).text());
		var itemCallNumber = $("td.itemcallnumber",tempHtml).text();
		var Name = $("td.status a",tempHtml).text();
		var copyNumber = $("td.copynumber",tempHtml).text();

		if( Name == "" )
		   Name = 'Item not checkout';
		
		var data = new Array(
						      Name,
						      itemCallNumber,
						      copyNumber,
						      $("#barcode").val(),
						      title,
						      location,
						      libraryLocation
						    );

		// parse library location to Print function
		PrintTools.libraryLocation = libraryLocation;

		return data;
	};

	

	BookDeliveryTools.mapLocation = function(location){

		switch(location.trim())
		{
			case 'Boonchoo Treethong Library, Lampang Campus' :return 'tulampang';break;
			case 'Faculty of Journalism and Mass Communication Library':return 'tujc';break;
			case 'Nongyao Chaiseri Library':return 'tumed';break;
			case 'Pridi Banomyong Library' :return 'tupridi';
			case 'Professor Direck Jayanama Library' :return 'tupol';break;
			case 'Professor Sangvian Indaravijaya Library' :return 'tuba';break;
			case 'Puey Ungphakorn Library (Econ.)' :return 'tuecon';break;
			case 'Puey Ungphakorn Library, Rangsit Campus' :return 'tupuey';break;
			case 'Sanya Dharmasakti Library' :return 'tulaw';break;
			case 'Thammasat Library, Pattaya Campus' :return 'tupattaya';break;
			default :return location.trim();
		}
	};

	

	

	BookDeliveryTools.enterEvent = function(event){
		if (event.keyCode == 13 || event.which == 13) 
		  return true;
	    else
	      return false;
	};

	
	BookDeliveryTools.disableController = function(){
		
		//Display spinner
		$('#btn-fade').click();
		var opts = {
					  lines: 13, // The number of lines to draw
					  length: 30, // The length of each line
					  width: 10, // The line thickness
					  radius: 30, // The radius of the inner circle
					  corners: 1, // Corner roundness (0..1)
					  rotate: 0, // The rotation offset
					  direction: 1, // 1: clockwise, -1: counterclockwise
					  color: '#fafafa', // #rgb or #rrggbb or array of colors
					  speed: 1, // Rounds per second
					  trail: 60, // Afterglow percentage
					  shadow: false, // Whether to render a shadow
					  hwaccel: false, // Whether to use hardware acceleration
					  className: 'spinner', // The CSS class to assign to the spinner
					  zIndex: 2e9, // The z-index (defaults to 2000000000)
					  top: '200px', // Top position relative to parent
					  left: '270px' // Left position relative to parent 
					};
		var target = document.getElementById('spinner');
		var spinner = new Spinner(opts).spin(target);
	};

	BookDeliveryTools.enableController = function(){

		$('#btn-back').click();
		$('#barcode').val("");//clear input 
	};


	BookDeliveryTools.init = function(){

        $('#btn-find').click(function(event) {
        	BookDeliveryTools.disableController();
        	event.preventDefault();
            BookDeliveryTools.getInfoFromBarcode();
            BookDeliveryTools.enableController();
		});

		$('#barcode').keypress(function(event) {
			if( BookDeliveryTools.enterEvent(event) ) 
			{
			   BookDeliveryTools.disableController(); 
			   event.preventDefault();
		       BookDeliveryTools.getInfoFromBarcode();
		       BookDeliveryTools.enableController();
		    }
		});
	};

