var ExportToTextTools = function(){};
    
    

	ExportToTextTools.newTab = function(barcodeArray){
        var data = "<html>"+
		"<head>"+
		"<meta name = 'viewport' content = 'width=device-width,initial-scale=1.0' charset='UTF-8'>"+ 
		"<script src='js/jquery-2.1.1.min.js'></script>"+
		"<script src='js/app/authentication/tools.js'></script>"+
	    "<script src='js/app/bookdelivery/tools.js'></script>"+
	    "<script src='js/app/authentication/tools.js'></script>"+
	    "<script src='js/app/export-to-text/tools.js'></script>"+
	    "<script src='js/app/print/tools.js'></script>"+
	    "<script src='js/app/table/tools.js'></script>"+
		"</head>"+
		"<title>Export Barcode</title>"+
		"<textarea id = 'barcode' style='width:500px;height:500px;;margin:10px;paddind:10px'>";
		var bc = "";
		for(var x = 0;x < barcodeArray.length;x++) 
		{
			bc += barcodeArray[x]+"\n";
		}       
		data += bc+"</textarea><br><button id = 'btn-download'>Download Barcode as text</button>";
		data += "<script>ExportToTextTools.init();</script>";
		var myWindow = window.open("",'_blank'); 
		myWindow.document.write(data);
	};

	ExportToTextTools.getBarcodeFromTable = function(){
        
		var barcodeArray = [];
		var row = $('tr'); 
        row.each(function(){
            barcodeArray.push( $('td',$(this)).eq(4).text() );
        });
        
       
        return barcodeArray;
	};

	ExportToTextTools.downloadBarcode = function(){

        var textToWrite = "";
		var textToWrite = $('textarea#barcode').val().replace(/\n/g,'\r\n');
		var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
		var fileNameToSaveAs = "barcode";
		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.innerHTML = "Download File";
		
		if (window.webkitURL != null)
		{
			downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
		}
		else
		{
			downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
			downloadLink.style.display = "none";
			document.body.appendChild(downloadLink);
		}
		downloadLink.click();
	};

	ExportToTextTools.init = function(){
        $('#btn-export').click(function(event) {
            event.preventDefault();
			var barcodeArray = ExportToTextTools.getBarcodeFromTable();
			ExportToTextTools.newTab(barcodeArray);
		});

		$('#btn-download').click(function(event) {
            event.preventDefault();
			ExportToTextTools.downloadBarcode();
		});
	};
