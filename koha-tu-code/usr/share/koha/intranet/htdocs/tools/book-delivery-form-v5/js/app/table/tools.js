var TableTools = function(){};
    

    TableTools.setTableTitle = function(libraryLocation){
        if( libraryLocation == "" )
			$('#topic').append("หนังสือ Book Delivery");
		else
		{
			$('#topic').empty();
			$('#topic').append("หนังสือ Book Delivery"+libraryLocation);
		}
			
	};

	TableTools.insertDataToTable = function(data){
        
        var tr = document.createElement("Tr");
		tr.setAttribute("align", "center");
		tr.setAttribute("padding", "5px");
		document.getElementById("body").appendChild(tr);

		var td1 = document.createElement("TD");
		var c1 = document.createTextNode(document.getElementById('table').rows.length-1);
		td1.appendChild(c1);
		tr.appendChild(td1);

		var td2 = document.createElement("TD");
		var c2 = document.createTextNode(data[0].trim());
		td2.appendChild(c2);
		tr.appendChild(td2);

		var td3 = document.createElement("TD");
		var c3 = document.createTextNode(data[1]);
		td3.appendChild(c3);
		tr.appendChild(td3);

		var td4 = document.createElement("TD");
		var c4 = document.createTextNode(data[2]);
		td4.appendChild(c4);
		tr.appendChild(td4);

		var td5 = document.createElement("TD");
		td5.setAttribute("id", "bc");
		var c5 = document.createTextNode(data[3]);
		td5.appendChild(c5);
		tr.appendChild(td5);

		var td6 = document.createElement("TD");
		var c6 = document.createTextNode(data[4]);
		td6.appendChild(c6);
		tr.appendChild(td6);

		var td7 = document.createElement("TD");
		var c7 = document.createTextNode(data[5]);
		td7.appendChild(c7);
		tr.appendChild(td7);

		var td8 = document.createElement("TD");
		var c8 = document.createTextNode("");
		td8.appendChild(c8);
		tr.appendChild(td8);

		var button = document.createElement("BUTTON");
		button.setAttribute("onclick", "TableTools.deleteRow(this)");
		var ch1 = document.createTextNode("Delete");
		button.appendChild(ch1);
		td8.appendChild(button);

		TableTools.checkNameBlank();
		TableTools.setTableTitle(data[6]);
	};

	TableTools.reOrder = function(){

        for (var i = 1; i < document.getElementById('table').rows.length; i++){ 
			var row = document.getElementById("table").rows[i].cells;
			row[0].innerHTML=i;
		}
    }

    TableTools.deleteRow = function(row){
        
		var numRow = row.parentNode.parentNode.rowIndex;
		document.getElementById('table').deleteRow(numRow);

		for (var i = numRow; i < document.getElementById('table').rows.length; i++){ 
			var rowDel = document.getElementById("table").rows[i].cells;
			rowDel[0].innerHTML=i;
		}

    };

    TableTools.checkNameBlank = function(){
        $("td").filter(function() {return $(this).text() == 'Item not checkout';}).css( "background", "rgb(255,15,15)" );
	};
