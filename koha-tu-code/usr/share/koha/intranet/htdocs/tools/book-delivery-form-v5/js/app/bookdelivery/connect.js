var BookDeliveryConnect = function() {
};
	
	BookDeliveryConnect.get = function() 
    {  
    	var link = "/cgi-bin/koha/catalogue/search.pl?idx=bc&q="+$('#barcode').val();
		$.ajax({
			url:link,
			async: false,
			type: "GET",
			data:{},
			dataType:'html',
			timeout: 200000,
			success: function(data)
			{
                if(AuthenticationTools.checkLogin(data))
                TableTools.insertDataToTable(BookDeliveryTools.extractDataFromHtml(data)); 
			},
			error:function(error)
			{console.log("error"+JSON.stringify(error));}
		});
    };

